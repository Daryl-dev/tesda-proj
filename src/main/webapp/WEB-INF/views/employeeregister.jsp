<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MINDTECH Registration form</title>

	<style>
		body{
			height: 100vh;
			background: linear-gradient(to right top, #65dfc9, #6cdbeb)
		
		}
		.container {
			background: white;
			width: 50%;
			min-height: 80vh;
			background: linear-gradient(to right bottom, 
				rgba(255, 255, 255, 0.5),
				rgba(255, 255, 255, 0.2)
				);
			margin: 80px auto;
			border-radius: 20px;
			display: flex;
			justify-content: center;
			align-items: center;
			flex-direction: column;
			z-index: 2;
			backdrop-filter: blur(2rem)
		}
		.circle1,
		.circle2 {
			background: white;
			background: linear-gradient(to right bottom, 
				rgba(255, 255, 255, 0.6),
				rgba(255, 255, 255, 0.2)
				);
			height: 10rem;
			width: 10rem;
			border-radius: 50%;
			position: absolute;
			z-index: -1;
		}
		.circle1 {
			top: 10px;
			left: 240px;
		}
		.circle2 {
			bottom: 0;
			right: 240px;
		}
		
		
		h1{
			margin: 0;
		}
		h2{
			margin-top: 0;
		}
		.button {
			width: 100%;
			display: flex;
			justify-content: center;
		}
		.button input {
			width: 30%;
			padding: 5px;
			margin: 10px 10px;
		}
		
	</style>
</head>
<body>
	<div class='container' >
		
			<h1>MINDTECH</h1>
			<h2>Learners Registration Form</h2>
	
			<form action="<%= request.getContextPath() %>/register" method="post">
				<table>
					<tr>
						<td>First Name</td>
						<td><input type="text" name="firstName" placeholder="First name"></td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td><input type="text" name="lastName" placeholder="Last name"></td>
					</tr>
					<tr>
						<td>User name</td>
						<td><input type="text" name="username" placeholder="User name"></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" name="password" placeholder="password"></td>
					</tr>
					<tr>
						<td>Address</td>
						<td><input type="text" name="address" placeholder="Municipality/Province"></td>
					</tr>
					<tr>
						<td>Contact no.</td>
						<td><input type="text" name="contact" placeholder="Contact Number"></td>
					</tr>
				</table>
				
				<div class="button">
					<input class="submit" type="submit" value="Submit" id="submit">
					<input class="reset" type = "reset" value="Reset" id="reset">
				</div>
			</form>
	</div>
	
	<div class="circle1"></div>
	<div class="circle2"></div>
</body>
</html>